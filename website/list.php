<?php
// lists all packages.

session_start();

# insert navbar.
include 'includes/db.php';

if (isset($_GET["pkg"])) {
	$pkg = mysqli_real_escape_string($mysqli, $_GET["pkg"]);
	$result = mysqli_query($mysqli, "SELECT name,description FROM Packages WHERE name LIKE '%$pkg%';");
} else {
	$result = mysqli_query($mysqli, "SELECT name,description FROM Packages;");
}


if (mysqli_num_rows($result) == 0) {
	echo json_encode(array());
	exit();
}

$res = array();

while ($a = mysqli_fetch_assoc($result)) {
	$res[] = array(
		"name" => $a["name"],
		"description" => $a["description"]
	);
}

echo json_encode($res);

?>
