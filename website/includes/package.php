<?php

function pprint($a) {
	global $mysqli;
	$name = $a["name"];
	if (! $name) {
		die ("No Name found in print.");
	}
	// escape string.
	$name = escape_xss(mysqli_real_escape_string($mysqli, $name));
	$result = <<<EOF
	<h3> $name </h3>
	EOF;
	if (isset($a["description"])) {
		$x = escape_xss($a["description"]);
		$result .= <<<EOF
		<br>
		<div class="m-1">
			<h4> Description: </h4>
			<p class="d-flex border border-info rounded p-1 bg-light">
				$x
			</p>
		</div>
		EOF;	
	}
	if (isset($a["upstream"])) {
		$x = escape_xss($a["upstream"]);
		$result .= <<<EOF
		<br>
		<div class="m-1">
			<h4> Upstream: </h4>
			<a href="$x" target="new"> $x </a>
		</div>
		EOF;
	}
	if (isset($a["env"]) && $a["env"]) {
		$x = escape_xss($a["env"]);
		$x = str_replace("\r\n", "<br>", $x);
		$result .= <<<EOF
		<br>
		<div class="m-1">
			<h4> Env: </h4>
			<code class="d-flex border border-info rounded p-1 bg-light">
				$x
			</code>
		</div>
		EOF;
	}
	if (isset($a["install"]) && $a["install"]) {
		$x = escape_xss($a["install"]);
		$x = str_replace("\r\n", "<br>", $x);
		$result .= <<<EOF
		<br>
		<div class="m-1">
			<h4> Install: </h4>
			<code class="d-flex border border-info rounded p-1 bg-light">
				$x
			</code>
		</div>
		EOF;
	}
	$buttons = <<<EOF
	<a href="/json.php?pkg=$name" class="btn btn-outline-primary" role="button">
		JSON
	</a>
	EOF;
	if (isset($_SESSION["admin"])) {
		$buttons .= <<<EOF
		<a href="/edit.php?pkg=$name" class="btn btn-outline-primary" role="button">
			Edit
		</a>
		<a href="/remove.php?pkg=$name" class="btn btn-outline-danger" role="button">
			Delete
		</a>
		EOF;
	}
	$result .= <<<EOF
	<br>
	<div class="m-1 d-flex justify-content-evenly">
		$buttons
	</div>
	EOF;
	echo <<<EOF
	<div class="border border-info rounded m-3 p-3" id="$name">
		$result
	</div>
	EOF;
}
?>
