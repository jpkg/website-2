<?php
$server = 'localhost';
$username = 'jpkg';
$password = '';

$mysqli = new mysqli($server, $username, $password, "jpkg");

if ($mysqli->connect_error) {
	die ("Could not establish connection: " . $mysqli->connect_error);
}

function admin_hash() {
	global $mysqli;
	$res = mysqli_query($mysqli, "SELECT * FROM Admin;");
	if (! $res || mysqli_num_rows($res) == 0) {
		return NULL;
	} else {
		$x = mysqli_fetch_assoc($res);
		return $x["hash"];
	}
}

function write_passwd($password) {
	global $mysqli;
	$hash = password_hash($password, PASSWORD_BCRYPT);
	$hash = mysqli_real_escape_string($mysqli,$hash);
	$result=mysqli_query($mysqli, "INSERT Admin(hash) VALUES ('$hash')");
	if ($result !== TRUE) {
		echo "<br> DB Error. ".$mysqli->error;
	}
}

?>
