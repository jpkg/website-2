<!-- Editing form -->
<?php
if (! isset($values)) {
	$values = array(
		"description"=> "",
		"upstream"=> '',
		"env"=> '',
		"install"=> ''
	);
}
?>
<div class="container-fluid row">
	<h1 class="p-2"> <?php echo $prompt ?> </h1>
	<form action="<?php echo $url?>" method="POST">
		<div class="row p-2">
			<label for="name" class="form-label col-3 p-2"> Name </label>
			<input type="text" <?php if ($readonly) { echo "readonly"; }?>
				class="col form-control<?php if ($readonly) { echo "-plaintext"; }?>" 
				<?php if (isset($pkg)) {echo "value='$pkg'";} ?>
				name="name"></input>
		</div>

		<div class="row p-2">
			<label for="description" class="form-label col-3 p-2"> Description </label>
			<input type="text" class="form-control col" rows="3" name="description"
			value="<?php echo $values["description"]?>"></input>
		</div>

		<div class="row p-2">
			<label for="upstream" class="form-label col-3 p-2"> Upstream </label>
			<input type="text" class="form-control col" name="upstream"
			value="<?php echo $values["upstream"]?>"></input>
		</div>

		<div class="row p-2">
			<label for="env" class="form-label col-3 p-2"> Env </label>
			<textarea class="form-control col" rows="5" name="env"
			><?php echo $values["env"] ?></textarea>
		</div>

		<div class="row p-2">
			<label for="install" class="form-label col-3 p-2"> Install </label>
			<textarea class="form-control col" rows="5" name="install"
			><?php echo $values["install"] ?></textarea>
		</div>

		<button type="submit" class="btn btn-outline-primary">
			Submit
		</button>
	</form>
<?php
if (isset($error)) {
	echo <<<EOF
	<div class="m-3 p-2 border border-danger rounded text-danger">
		$error
	</div>
	EOF;
}
?>
</div>
