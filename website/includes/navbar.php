<?php
function gen_navbar($ar) {

	$res = "";
	$admin = "";
	foreach($ar as $title=> $url) {
		$res .= <<<EOT
			<a href="$url" class="btn btn-outline-primary m-2" role=button>
				$title
			</a>
		EOT;
	}
	return <<<EOT
<nav class="navbar navbar-expand-sm navbar-light bg-light">
  <div class="container-fluid">
    <div class="navbar-header">
		<a class="navbar-brand" href="/index.php">
			JPK
			<img src="/images/pixel.png" width="36px" height="30px"/>
		</a>
    </div>
    <ul class="nav navbar-nav">
		<li>
		  $res
		</li>
    </ul>
  </div>
</nav>
EOT;

}

$navbar = array("Packages"=> "/packages.php");
if (isset($_SESSION["admin"])) {
	$navbar["Log out"] = "/logout.php";
} else {
	$navbar["Admin Login"] = "/login.php";
}

echo gen_navbar($navbar);

?>
