<!DOCTYPE html>
<?php
session_start();
?>
<html>
	<head>
<?php 
include 'includes/headers.php';
include 'includes/db.php';
?>
	<head>
	<body>

<?php
# insert navbar.
include 'includes/navbar.php';
?>
		<h1 class="p-3"> New Admin Password </h1>
		<div class="container-fluid row">
			<div class="col col-md-5">
				<div class="mx-3 border border-outline-info rounded bg-light">
					<form action="/admin_init.php" method="POST" class="p-3">
						<label for="password" class="form-label m-1"> Password: </label>
						<input type="password" class="form-control m-1" name="password">
						<br>
						<label for="repeat" class="form-label m-1"> Password Repeat </label>
						<input type="password" class="form-control m-1" name="repeat">
						<br>
						<button type="submit" class="btn btn-info m-1">
							Generate Admin Password
						</button>
					</form>
				</div>
			</div>
			<?php
			if (admin_hash()) {
				header("Location: /login.php");
				die();
			}
			function fail($msg) {
				echo <<<EOF
					<div class="col col-md-3">
						<div class="mx-3 border border-danger bg-danger rounded d-flex">
							<h3 class="m-1 text-center text-white">
								$msg
							</h3>
						</div>
					</div>
				EOF;
			}
			function success($msg) {
				echo <<<EOF
					<div class="col col-md-3">
						<div class="mx-3 border border-success bg-success rounded d-flex">
							<h3 class="m-1 text-center text-white">
								$msg
							</h3>
						</div>
					</div>
				EOF;
			}
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				# admin password
				if ($_POST["password"] !== $_POST["repeat"]) {
					fail("Password and Repeat do not match.");
				} elseif(strlen($_POST["password"]) < 8) {
					fail("Password must be at least 8 chars.");
				} else {
					write_passwd($_POST["password"]);
					success("Password successfully written.");
					$_SESSION["admin"] = "admin";
				}
			}
			?>
		</div>
	</body>
</html>
