<!DOCTYPE html>
<?php
session_start();
?>
<html>
	<head>
		<?php 
		include 'includes/headers.php';
		include 'includes/db.php';
		?>
	<head>
	<body>
<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if (strlen($_POST["password"]) > 4096) {
		$auth = FALSE;
	}
	else {
		$auth = password_verify($_POST["password"], admin_hash());
		if ($auth) {
			$_SESSION["admin"] = "admin";
		}
	}
}
# insert navbar.
include 'includes/navbar.php';
?>
		<h1 class="p-3"> Admin Login </h1>
		<div class="container-fluid row">
			<div class="col col-md-4">
				<div class="mx-3 border border-outline-info bg-light">
					<form action="/login.php" method="POST" class="p-3">
						<label for="password" class="form-label m-1"> Password: </label>
						<input type="password" class="form-control m-1" name="password">
						<br>
						<button type="submit" class="btn btn-info m-1">
							Auth as Admin
						</button>
					</form>
				</div>
			</div>
			<?php
			if (! admin_hash()) {
				header("Location: /admin_init.php");
				die();
			}
			function fail($msg) {
				echo <<<EOF
					<div class="col col-md-3">
						<div class="mx-3 border border-danger bg-danger rounded d-flex">
							<h3 class="m-1 text-center text-white">
								$msg
							</h3>
						</div>
					</div>
				EOF;
			}
			function success($msg) {
				echo <<<EOF
					<div class="col col-md-3">
						<div class="mx-3 border border-success bg-success rounded d-flex">
							<h3 class="m-1 text-center text-white">
								$msg
							</h3>
						</div>
					</div>
				EOF;
			}
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				# authenticate.
				if ($auth) {
					success("Logged in as admin.");
				} else {
					fail("password invalid (or longer than 4096 chars).");
				}
			}
			?>
		</div>
	</body>
</html>
