<!DOCTYPE html>
<?php
session_start();
?>
<html>
	<head>
<?php 
include 'includes/headers.php'
?>
	<head>
	<body>
<?php
# insert navbar.
include 'includes/db.php';
include 'includes/navbar.php';
include 'includes/package.php';

if (! isset($_SESSION["admin"])) {
	header("Location: errors/403.php");
	die();
}

// create new package.
$error = NULL;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$name = trim(mysqli_real_escape_string($mysqli, $_POST["name"]));
	$us = trim(mysqli_real_escape_string($mysqli, $_POST["upstream"]));
	$desc = trim(mysqli_real_escape_string($mysqli, $_POST["description"]));
	$env = trim(mysqli_real_escape_string($mysqli, $_POST["env"]));
	$install = trim(mysqli_real_escape_string($mysqli, $_POST["install"]));

	$query = mysqli_query($mysqli, "SELECT * FROM Packages WHERE name='$name'");
	if (mysqli_num_rows($query) == 0 && strlen($name) > 0) {
		mysqli_query($mysqli, 
			"INSERT Packages(name, upstream, description, env, install)
			VALUES('$name', '$us', '$desc', '$env', '$install')");
		header("Location: /packages.php");
		die();
	} else {
		$error = "'$name' already exists or '$name' has no characters.";
	}
}


$prompt = "Create a new Package";
$url = "/create.php";
$readonly = FALSE;

include_once 'includes/edit.php';
?>
	</body>
</html>
