<?php
session_start();

# insert navbar.
include 'includes/db.php';

if (! isset($_GET["pkg"])) {
	http_response_code(400);
	die();
}

$pkg = mysqli_real_escape_string($mysqli, $_GET["pkg"]);

$result = mysqli_query($mysqli, "SELECT * FROM Packages WHERE name='$pkg';");
if (mysqli_num_rows($result) == 0) {
	header("Location: /errors/404.php");
	die();
}

$a = mysqli_fetch_assoc($result);
$a = str_replace("\r\n", '; ', $a);

$res = array(
	"name" => $a["name"],
	"env" => $a["env"],
	"install" => $a["install"]
);

echo json_encode($res);

?>
