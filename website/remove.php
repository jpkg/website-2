<?php
session_start();

# insert navbar.
include 'includes/db.php';

if (! $_SESSION["admin"]) {
	header("Location: /errors/403.php");
	die();
}

if (! isset($_GET["pkg"])) {
	http_response_code(400);
	die();
}

$pkg = mysqli_real_escape_string($mysqli, $_GET["pkg"]);

mysqli_query($mysqli, "DELETE FROM Packages WHERE name='$pkg'");

header("Location: /packages.php");
die();

?>
