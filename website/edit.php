<!DOCTYPE html>
<?php
session_start();
?>
<html>
	<head>
<?php 
include 'includes/headers.php'
?>
	<head>
	<body>
<?php
# insert navbar.
include 'includes/db.php';
include 'includes/navbar.php';
include 'includes/package.php';

if (! isset($_SESSION["admin"])) {
	header("Location: errors/403.php");
	die();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	// update.
	$name = mysqli_real_escape_string($mysqli, $_POST["name"]);
	$us = mysqli_real_escape_string($mysqli, $_POST["upstream"]);
	$desc = mysqli_real_escape_string($mysqli, $_POST["description"]);
	$env = mysqli_real_escape_string($mysqli, $_POST["env"]);
	$install = mysqli_real_escape_string($mysqli, $_POST["install"]);

	mysqli_query($mysqli,
	"DELETE FROM Packages WHERE name='$name'");

	mysqli_query($mysqli, 
		"INSERT Packages(name, upstream, description, env, install)
		VALUES('$name', '$us', '$desc', '$env', '$install')");
}

// always sanitize before raw sql query.
$pkg = mysqli_real_escape_string($mysqli, $_GET["pkg"]);
$pkg = escape_xss($pkg);

$result = mysqli_query($mysqli, "SELECT * FROM Packages WHERE name='$pkg'");

$message = "";

if (mysqli_num_rows($result) == 0) {
	header("Location: /errors/404.php");
	die();
}

$values = mysqli_fetch_assoc($result);

$prompt = "Edit $pkg";
$url = "/edit.php?pkg=$pkg";
$readonly = TRUE;

include_once 'includes/edit.php';
?>
</body>
</html>
