<!DOCTYPE html>
<?php
session_start();
?>
<html>
	<head>
		<?php include 'includes/headers.php'?>
	</head>
	<body>
<?php
# insert navbar.
include 'includes/navbar.php';
include 'includes/db.php';
include 'includes/package.php';

$result = mysqli_query($mysqli, "SELECT * FROM Packages;");

$rows = mysqli_num_rows($result);
$a = ceil($rows/2);
$b = $rows - $a;

?>
		<div class="container-fluid">
			<div class="d-flex justify-content-left p-3">
				<h1 class="m-2"> Packages </h1>
				<?php
				if (isset($_SESSION["admin"])) {
					echo <<<EOF
					<a href="/create.php" class="btn btn-outline-primary m-3" role="button">
						Create New Package
					</a>
				EOF;
				}
				?>
			</div>
			<div class="row">
				<div class="col col-md-5 m-3">
					<?php
					for ($i = 0; $i < $a; $i++) {
						$x = mysqli_fetch_assoc($result);
						echo pprint($x);
					}
					?>
				</div>
				<div class="col col-md-5 m-3">
					<?php
					for ($i = 0; $i < $b; $i++) {
						$x = mysqli_fetch_assoc($result);
						echo pprint($x);
					}
					?>
				</div>
			</div>
		</div>
	</body>
</html>
