<!DOCTYPE html>
<?php
session_start();
?>
<html>
	<head>
		<?php include 'includes/headers.php'?>
	<head>
	<body>
<?php
# insert navbar.
include 'includes/navbar.php';
include 'includes/db.php';
include 'includes/package.php';

function contact($ar) {
	foreach ($ar as $name=>$url) {
		echo <<<EOF
		<a 
		href="$url" target='new'
		class="btn btn-outline-primary m-2" role='button'
		> $name </a>
		EOF;
	}
}

$result = mysqli_query($mysqli, "SELECT name, upstream FROM Packages LIMIT 3;");

?>
		<div class="container-fluid row">
			<div class="col col-md-5 m-3">
				<div class="border border-info m-3 rounded bg-light p-3">
					<h1>JPK</h1>
					<p>
						This website creates an API that the JPK package manager uses to reliably
						install scripts.  It's design is meant for exactly one person to upload all of
						their project packages.
					</p>
				</div>

				<div class="border border-info m-3 rounded bg-light p-3">
					<h2> Contacts </h2>
					<div class="d-flex justify-content-evenly">
						<?php
						contact(array(
							"Github" => "https://github.com/junikimm717",
							"Gitlab" => "https://gitlab.com/junikimm717",
							"Personal Website" => "https://junickim.me",
							"Email" => "mailto:junikimm717@gmail.com"
						));
						?>
					</div>
				</div>
			</div>
			<div class="col col-md-5 m-3">
				<div class="border border-info m-3 rounded bg-light p-3">
					<h2> Installation </h2>
					<p>
						The JPK Package Manager is yet to be released. Once
						released, an installation script will be displayed.
					</p>
				</div>
			</div>
		</div>
	</body>
</html>
