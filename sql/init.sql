
CREATE DATABASE IF NOT EXISTS jpkg;
GRANT ALL ON jpkg.* TO 'jpkg'@'localhost';


use jpkg;

-- clear tables

DROP TABLE Admin;
DROP TABLE Packages;

-- for storing admin password hash.
CREATE TABLE Admin (
	hash VARCHAR(255) NOT NULL,
	PRIMARY KEY(hash)
);

-- table of packages.
CREATE TABLE Packages (
	name VARCHAR(255) NOT NULL,
	upstream VARCHAR(255) NOT NULL,
	description TEXT NOT NULL,
	env TEXT NOT NULL,
	install TEXT NOT NULL,
	PRIMARY KEY (name)
);
